.PHONY: glide

REVISION := $(shell git rev-parse HEAD)
build: gox
	gox -osarch="windows/amd64 linux/amd64 linxu/arm linux/arm64"  -verbose -output="dist/{{.Dir}}_{{.OS}}_{{.Arch}}_$(REVISION)"

gox:
ifeq ($(shell command -v gox 2> /dev/null),)
	go get -u github.com/mitchellh/gox
endif
clean:
	@rm -rf dist/hoge*